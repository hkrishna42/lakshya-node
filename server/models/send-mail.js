'use strict';

module.exports = function(Sendmail) {
    Sendmail.sendEmail = function(to,subject,body,filename,path,cb) {

        Sendmail.app.models.sendMail.send({
          to: to,
          from: 'system@vasturealestate.in',
          subject: subject,
          text: body,
          html: '<p>'+body+'</p>',
          attachments :[{finename:filename,path:path}]
        }, function(err, mail) {
            if(err){
                cb(err);
            }
            else{
                console.log(mail);
                cb(null,{"success":true});
            }
          
          
        });
    }
    Sendmail.sendEmailWithoutAttachment = function(to,subject,body,cb) {
      Sendmail.app.models.sendMail.send({
        to: to,
        from: 'system@vasturealestate.in',
        subject: subject,
        text: body,
        html: '<p>'+body+'</p>'
        //attachments :[{finename:filename,path:path}]
      }, function(err, mail) {
          if(err){
              cb(err);
          }
          else{
              console.log(mail);
              cb(null,{"success":true});
          }
        
        
      });
    }

    Sendmail.sendEmailWithoutText = function(to,subject,body,cb) {
      Sendmail.app.models.sendMail.send({
        to: to,
        from: 'system@vasturealestate.in',
        subject: subject,
        text: '',
        html: body
        //attachments :[{finename:filename,path:path}]
      }, function(err, mail) {
          if(err){
              cb(err);
          }
          else{
              console.log(mail);
              cb(null,{"success":true});
          }
        
        
      });
    }

    Sendmail.sendIndividualLeadMail = function(to,subject,body,attachment,cb){
      Sendmail.app.models.sendMail.send({
        to : to,
        from: 'system@vasturealestate.in',
        subject: subject,
        text: body,
        html: '<p>'+body+'</p>',
        attachments :attachment
      }, function(err, mail) {
          if(err){
              cb(err);
          }
          else{
              console.log(mail);
              cb(null,{"success":true});
          }

      });
    }
    Sendmail.remoteMethod('sendIndividualLeadMail',{
      accepts: [
        {
          "arg": "to",
          "type": "string",
          "description": ""
        },
        {
          "arg": "subject",
          "type": "string",
          "description": ""
          
        },
        {
          "arg": "body",
          "type": "string",
          "description": ""
          
        },
        {
          "arg": "attachment",
          "type": "array",
          "description": ""
          
        }
      ],
      returns: {
          "arg": "result",
          "type": "object",
          "root": true,
          "description": ""
        },
        http: [
        {
          "path": "/sendIndividualLeadEmail",
          "verb": "post"
        }
      ]
    });
    Sendmail.remoteMethod('sendEmail', {
        accepts: [
          {
            "arg": "to",
            "type": "string",
            "description": "",
            'http': {source: 'query'}
          },
          {
            "arg": "subject",
            "type": "string",
            "description": "",
            'http': {source: 'query'}
          },
          {
            "arg": "body",
            "type": "string",
            "description": "",
            'http': {source: 'query'}
          },
          {
            "arg": "filename",
            "type": "string",
            "description": "",
            'http': {source: 'query'}
          },
          {
            "arg": "path",
            "type": "string",
            "description": "",
            'http': {source: 'query'}
          },
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/sendEmail",
            "verb": "get"
          }
        ]
    });

    Sendmail.remoteMethod('sendEmailWithoutAttachment', {
      accepts: [
        {
          "arg": "to",
          "type": "string",
          "description": "",
          'http': {source: 'query'}
        },
        {
          "arg": "subject",
          "type": "string",
          "description": "",
          'http': {source: 'query'}
        },
        {
          "arg": "body",
          "type": "string",
          "description": "",
          'http': {source: 'query'}
        }
      ],
      returns: {
          "arg": "result",
          "type": "object",
          "root": true,
          "description": ""
        },
        http: [
        {
          "path": "/sendEmailWithoutAttachment",
          "verb": "get"
        }
      ]
  });
  /*Sendmail.remoteMethod('sendEmailWithoutText', {
    accepts: [
      {
        "arg": "to",
        "type": "string",
        "description": "",
        'http': {source: 'query'}
      },
      {
        "arg": "subject",
        "type": "string",
        "description": "",
        'http': {source: 'query'}
      },
      {
        "arg": "body",
        "type": "string",
        "description": "",
        'http': {source: 'query'}
      }
    ],
    returns: {
        "arg": "result",
        "type": "object",
        "root": true,
        "description": ""
      },
      http: [
      {
        "path": "/sendEmailWithoutText",
        "verb": "post"
      }
    ]
});*/

Sendmail.remoteMethod('sendEmailWithoutText',{
  accepts: [
    {
      "arg": "to",
      "type": "string",
      "description": ""
    },
    {
      "arg": "subject",
      "type": "string",
      "description": ""
      
    },
    {
      "arg": "body",
      "type": "string",
      "description": ""
      
    }
  ],
  returns: {
      "arg": "result",
      "type": "object",
      "root": true,
      "description": ""
    },
    http: [
    {
      "path": "/sendEmailWithoutText",
      "verb": "post"
    }
  ]
  
});



};
