var loopback = require('loopback');
var boot = require('loopback-boot');
var fs = require('fs');
var http = require('http');
var https = require('https');
var sslConfig = require('./ssl-config');
//var admin = require('firebase-admin');
var app = module.exports = loopback();

app.start = function(httpOnly) {
  if (httpOnly === undefined) {
    httpOnly = process.env.HTTP;
  }
  var server = null;
  if (!httpOnly) {
    console.log(sslConfig.privateKey);
    /*var options = {
      key: sslConfig.privateKey,
      cert: sslConfig.certificate,
    };*/

var key = fs.readFileSync('/home/ubuntu/vasturealestate.in.key');
var cert = fs.readFileSync('/home/ubuntu/vasturealestate.in.crt');
var ca = fs.readFileSync( '/home/ubuntu/intermediate.crt' );

var options = {
key: key,
cert: cert,
ca: ca
};
    server = https.createServer(options, app);
  } else {
    server = http.createServer(app);
  }
  server.listen(app.get('port'), function() {
    var baseUrl = (httpOnly ? 'http://' : 'https://') + app.get('host') + ':' + app.get('port');
    app.emit('started', baseUrl);
    console.log('LoopBack server listening @ %s%s', baseUrl, '/');
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
  return server;
};

// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}


boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module){


    app.io = require('socket.io')(app.start(),{path:'/chat'});

    // handle incoming connections from clients
    app.io.sockets.on('connection', function(socket) {

        socket.emit('send id');
        //socket.emit('msg', { hello: 'world' });
        socket.on('connect to room', function (data) {
          //console.log(data);
          socket.join(data.groupId,function () {

          });
        });

        socket.on('message', function (data) {
          //console.log(data);
          socket.to(data.groupId).emit('reload',{asda:'ASda'});
        });

        socket.on('leave room', function (data) {
          //console.log(data);
          socket.to(data.groupId).emit('reload',{asda:'ASda'});
          socket.leave(data.groupId);
        });

    });

  }

});

