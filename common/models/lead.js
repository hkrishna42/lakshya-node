
var loopback = require('loopback');
var admin = require("firebase-admin");
var serviceAccount = require("../../lakshya-firebase.json");

module.exports = function(Lead) {
  
  var Lakshyaproperty = require('loopback').getModel('lakshyaProperty');
  var Zone = require('loopback').getModel('zone');
  var Admingroup = require('loopback').getModel('admin-group');
  var User = require('loopback').getModel('lakshyaUser');
  //var Leadstatus = require('loopback').getModel('leadStatus');
  //var Notification = require('loopback').getModel('notification');
  //var Notificationcollection = Notification.getDataSource().connector.collection(Notification.modelName);

  Lead.afterRemote ('openNewLead',function(context,remoteMethodOutput,next){
   
    //console.log(remoteMethodOutput);
    //console.log(context.result.users);
    var usersArray = [];
    usersArray = context.result.users.map(String);   
    //console.log(usersArray);

    recursiveNotification(0,usersArray,function(result){
        if(result){
          //callback(false);
          next();
        }
        /*else{
          next();
        }*/
    });
     
  });

  var recursiveNotification = function(index,usersArray,callback){
    
    let app = require('../../server/server');
   //console.log(usersArray[index]);
    if(index == usersArray.length){
      callback(true);
    }
    else{
      var payload = {
        notification: {
          title: "Lead Alert",
          body: "New lead created for your property"
        }
      };
      //console.log(usersArray[index]);
      //recursiveNotification(index+1,usersArray,callback);
      //console.log(app.models.notification);
      app.models.notification.find({},function(error,response){
        if(error){
          //console.log(error);
          callback(false);
        }
        else{

          //console.log(response);
          var loopIndex = 0;
          var notificationToken = [];
          while(loopIndex < response.length){
            if(response[loopIndex].userId == usersArray[index]){
              notificationToken.push(response[loopIndex].notificationToken);
              loopIndex = loopIndex + 1;
            }
            else{
              loopIndex = loopIndex + 1;
            }
            
          }

          //console.log(notificationToken);
          if(notificationToken.length != 0){
            admin.messaging().sendToDevice(notificationToken, payload)
            .then(function(response) {
              //console.log(response);
            })
            .catch(function(error) {
            //console.log(error);
            });
            recursiveNotification(index+1,usersArray,callback);
          }
          else{
            recursiveNotification(index+1,usersArray,callback);
          }
    
        }
      });
    }
    

  }


  
  var recursiveFindUser = function(index,userArray,seArray,otherUserArray,ownerArray,callback){
    if(userArray.length == index){
      callback(seArray,otherUserArray,ownerArray);
    }
    else{
      User.find({where:{"id":userArray[index].userId}},function(userError,userResponse){
        if(userError){
          callback(false,false,false);
        }
        else if(userResponse.length>0){
	console.log(userResponse,'users');
          if(userResponse[0].realm == "SE"){
            seArray.push(userResponse[0]);
            recursiveFindUser(index+1,userArray,seArray,otherUserArray,ownerArray,callback);
          }
          else if(userResponse[0].realm == "TL"){
            otherUserArray.push(userResponse[0]);
            recursiveFindUser(index+1,userArray,seArray,otherUserArray,ownerArray,callback);
          }
          else if(userResponse[0].realm == "Owner"){
            ownerArray.push(userResponse[0]);
            recursiveFindUser(index+1,userArray,seArray,otherUserArray,ownerArray,callback);
          }
          else{
            recursiveFindUser(index+1,userArray,seArray,otherUserArray,ownerArray,callback);
          }
        }else{
	  recursiveFindUser(index+1,userArray,seArray,otherUserArray,ownerArray,callback);
	}
      });
    }
  }
   
	 Lead.openNewLead = function(propertyId, leadEmail, leadFirstName, leadContact, leadSourceName, leadLastName,  leadWebsiteForm, leadReferenceName,leadReferenceNumber,leadUserId,callback) {
    let app = require('../../server/server'); /** make sure the path to server is right **/

    //console.log(app.models.clusterManager) ;
		Lead.findOrCreate({where: { propertyId: propertyId, leadEmail: leadEmail,leadContact:leadContact }}, { propertyId: propertyId, leadEmail: leadEmail, leadFirstName:leadFirstName, leadContact: leadContact, leadSourceName: leadSourceName, leadLastName: leadLastName,  leadCurrentStatus: "open", leadWebsiteForm:leadWebsiteForm,leadReferenceName:leadReferenceName,leadReferenceNumber:leadReferenceNumber,leadUserId:leadUserId }, function(err, res){
		   	if (err) {
		 	 	  callback(err);
		   	}else{
            if(leadWebsiteForm == "enquire"){
              app.models.leadStatus.findOrCreate({where : {leadId : res.id}}, {leadFeedback:"Initial",leadSalesStage:"siteVisit",leadEnquiryStatus:"open",leadEnquiryReason:"",leadId:res.id,leadStatusCreatedOn:new Date(),leadStatusCreatedBy:leadUserId},function(leadStatusError,leadStatusResponse){
                Lakshyaproperty.find({where:{id:propertyId}},function(propertyError,propertyResponse){
                  if(propertyError){
                    callback(propertyError)
                  }
                  else{
                    Zone.find({where:{zipcodes : propertyResponse[0].propertyZipcode}},function(zoneError,zoneResponse){
                      if(zoneError){
                        callback(zoneError);
                      }
                      else{
                        app.models.clusterManager.find({where:{cmZoneId:zoneResponse[0].id}},function(cmError,cmResponse){
                          if(cmError){
                            callback(cmError);
                          }
                          else{
                            //console.log(cmResponse);
                            if(leadWebsiteForm == "chat"){
                              Admingroup.findOrCreate({where:{propertyId:propertyId, userId : cmResponse[0].cmId}},{propertyId:propertyId, userId : cmResponse[0].cmId},function(adminGroupError,adminGroupResponse){
                                if(adminGroupError){
                                  callback(adminGroupError);
                                }
                                else{
                                  User.find({where:{id: cmResponse[0].cmId}},function(userError,userResponse){
                                    app.models.assignlead.findOrCreate({where:{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id}},{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id, userType:"CM",userFirstName:userResponse[0].userFirstName,userUnique:cmResponse[0].cmUnique},function(assignleadError,assignleadResponse){
                                      if(assignleadError){
                                        callback(assignleadError);
                                      }
                                      else{
                                        Admingroup.find({include: 'lakshyaUser',where:{propertyId:propertyId}},function(findUsersError,findUsersResponse){
                                          if(findUsersError){
                                            callback(findUsersError);
                                          }
                                          else{
                                            if(findUsersResponse != null){
                                              //console.log("TL");
                                              //console.log(findUsersResponse);
                                              var seArray = [];
                                              var otherUserArray = [];
                                              var ownerArray = [];
                                              var countIndex = 0;
                                              
                                              recursiveFindUser(0,findUsersResponse,seArray,otherUserArray,ownerArray,function(seArray,tlArray,ownerArray){
                                                //console.log(seArray);
                                                if(seArray.length == 1){
                                                  app.models.salesExecutive.find({where:{seId:seArray[0].id}},function(seError,seResponse){
                                                    if(seError){
                                                      callback(seError);
                                                    }
                                                    else{
                                                      app.models.assignlead.findOrCreate({where:{propertyId,userId:seArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:seArray[0].id, leadId : res.id, userType:"SE",userFirstName:seArray[0].userFirstName,userUnique:seResponse[0].seUnique},function(assignSeError,assignSeResponse){
                                                        app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                          if(tlError){
                                                            callback(tlError);
                                                          }
                                                          else{
                                                            app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                              if(ownerArray.length != 0){
                                                                var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id,ownerArray[0].id];
                                                                callback(null,{"success":true,"users":usersTobeSent});
                                                              }
                                                              else{
                                                                var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id];
                                                                callback(null,{"success":true,"users":usersTobeSent});
                                                              }
        
                                                            });
                                                          }
                                                        });
                                                        //callback(null,{"success":true});
        
                                                      });
                                                    }
                       
                                                  });
                                                }
                                                else{
                                                  if(tlArray.length == 0){
                                                    if(ownerArray.length != 0){
                                                      var usersTobeSent = [cmResponse[0].cmId,ownerArray[0].id];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
                                                    else{
                                                      var usersTobeSent = [cmResponse[0].cmId];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
    
                                                  }
                                                  else{
    
                                                      app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                        if(tlError){
                                                          callback(tlError);
                                                        }
                                                        else{
                                                          app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                            var afterResult = [];
                                                            if(ownerArray.length != 0){
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,ownerArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
                                                            else{
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
    
                                                          });
                                                        }
                                                      });
                                                  }
          
                                                }
                                                
                                              });
                                            }
                                          }
                                        });
        
                                      }
                                      
                                    });
                                  });
                                  
                                  
                                }
                              });
      
                            }
                            else{
                            
                              User.find({where:{id: cmResponse[0].cmId}},function(userError,userResponse){
                                app.models.assignlead.findOrCreate({where:{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id}},{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id, userType:"CM",userFirstName:userResponse[0].userFirstName,userUnique:cmResponse[0].cmUnique},function(assignleadError,assignleadResponse){
                                  if(assignleadError){
                                    callback(assignleadError);
                                  }
                                  else{
                                    
                                    Admingroup.find({include: 'lakshyaUser',where:{propertyId:propertyId}},function(findUsersError,findUsersResponse){
                                      if(findUsersError){
                                        callback(findUsersError);
                                      }
                                      else{
                                        if(findUsersResponse != null){
                                          //console.log("TL");
                                          //console.log(findUsersResponse);
                                          var seArray = [];
                                          var otherUserArray = [];
                                          var ownerArray = [];
                                          var countIndex = 0;
                                          
                                          recursiveFindUser(0,findUsersResponse,seArray,otherUserArray,ownerArray,function(seArray,tlArray){
                                            //console.log(seArray);
                                            if(seArray.length == 1){
                                              app.models.salesExecutive.find({where:{seId:seArray[0].id}},function(seError,seResponse){
                                                if(seError){
                                                  callback(seError);
                                                }
                                                else{
                                                  if(tlArray.length == 0){
                                                    if(ownerArray.length != 0){
                                                      var usersTobeSent = [cmResponse[0].cmId,ownerArray[0].id];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
                                                    else{
                                                      var usersTobeSent = [cmResponse[0].cmId];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
        
                                                  }
                                                  else{
                                                    app.models.assignlead.findOrCreate({where:{propertyId,userId:seArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:seArray[0].id, leadId : res.id, userType:"SE",userFirstName:seArray[0].userFirstName,userUnique:seResponse[0].seUnique},function(assignSeError,assignSeResponse){
                                                      app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                        if(tlError){
                                                          callback(tlError);
                                                        }
                                                        else{
                                                          app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                            if(ownerArray.length != 0){
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id,ownerArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
                                                            else{
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
                                                            
                                                          });
                                                        }
                                                      });
                                                    //callback(null,{"success":true});
    
                                                    });
                                                  }
                                                }
                   
                                              });
                                            }
                                            else{
                                              if(tlArray.length == 0){
                                                if(ownerArray.length != 0){
                                                  var usersTobeSent = [cmResponse[0].cmId,ownerArray[0].id];
                                                  callback(null,{"success":true,"users":usersTobeSent});
                                                }
                                                else{
                                                  var usersTobeSent = [cmResponse[0].cmId];
                                                  callback(null,{"success":true,"users":usersTobeSent});
                                                }
    
                                              }
                                              else{
                                                app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                  if(tlError){
                                                    callback(tlError);
                                                  }
                                                  else{
                                                    app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                      if(ownerArray.length != 0){
                                                        var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,ownerArray[0].id];
                                                        callback(null,{"success":true,"users":usersTobeSent});
                                                      }
                                                      else{
                                                        var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id];
                                                        callback(null,{"success":true,"users":usersTobeSent});
                                                      }
    
                                                    });
                                                  }
                                                });
                                              }
                                            }
                                            
                                          });
                                        }
                                      }
                                    });
                                    
    
                                  }
                                  
                                });
    
                              });
                            }
                            
                          }
                          
                        });
                      }
                    });
                  }
                  
                
                });
              });
            }
            else{
              app.models.leadStatus.findOrCreate({where : {leadId : res.id}}, {leadFeedback:"open",leadSalesStage:"open",leadEnquiryStatus:"open",leadEnquiryReason:"",leadId:res.id,leadStatusCreatedOn:new Date(),leadStatusCreatedBy:leadUserId},function(leadStatusError,leadStatusResponse){
                Lakshyaproperty.find({where:{id:propertyId}},function(propertyError,propertyResponse){
                  if(propertyError){
                    callback(propertyError)
                  }
                  else{
                    Zone.find({where:{zipcodes : propertyResponse[0].propertyZipcode}},function(zoneError,zoneResponse){
                      if(zoneError){
                        callback(zoneError);
                      }
                      else{
                        app.models.clusterManager.find({where:{cmZoneId:zoneResponse[0].id}},function(cmError,cmResponse){
                          if(cmError){
                            callback(cmError);
                          }
                          else{
                            //console.log(cmResponse);
                            if(leadWebsiteForm == "chat"){
                              Admingroup.findOrCreate({where:{propertyId:propertyId, userId : cmResponse[0].cmId}},{propertyId:propertyId, userId : cmResponse[0].cmId},function(adminGroupError,adminGroupResponse){
                                if(adminGroupError){
                                  callback(adminGroupError);
                                }
                                else{
                                  User.find({where:{id: cmResponse[0].cmId}},function(userError,userResponse){
                                    app.models.assignlead.findOrCreate({where:{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id}},{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id, userType:"CM",userFirstName:userResponse[0].userFirstName,userUnique:cmResponse[0].cmUnique},function(assignleadError,assignleadResponse){
                                      if(assignleadError){
                                        callback(assignleadError);
                                      }
                                      else{
                                        Admingroup.find({include: 'lakshyaUser',where:{propertyId:propertyId}},function(findUsersError,findUsersResponse){
                                          if(findUsersError){
                                            callback(findUsersError);
                                          }
                                          else{
                                            if(findUsersResponse != null){
                                              //console.log("TL");
                                              //console.log(findUsersResponse);
                                              var seArray = [];
                                              var otherUserArray = [];
                                              var ownerArray = [];
                                              var countIndex = 0;
                                              
                                              recursiveFindUser(0,findUsersResponse,seArray,otherUserArray,ownerArray,function(seArray,tlArray,ownerArray){
                                                //console.log(seArray);
                                                if(seArray.length == 1){
                                                  app.models.salesExecutive.find({where:{seId:seArray[0].id}},function(seError,seResponse){
                                                    if(seError){
                                                      callback(seError);
                                                    }
                                                    else{
                                                      app.models.assignlead.findOrCreate({where:{propertyId,userId:seArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:seArray[0].id, leadId : res.id, userType:"SE",userFirstName:seArray[0].userFirstName,userUnique:seResponse[0].seUnique},function(assignSeError,assignSeResponse){
                                                        app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                          if(tlError){
                                                            callback(tlError);
                                                          }
                                                          else{
                                                            app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                              if(ownerArray.length != 0){
                                                                var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id,ownerArray[0].id];
                                                                callback(null,{"success":true,"users":usersTobeSent});
                                                              }
                                                              else{
                                                                var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id];
                                                                callback(null,{"success":true,"users":usersTobeSent});
                                                              }
        
                                                            });
                                                          }
                                                        });
                                                        //callback(null,{"success":true});
        
                                                      });
                                                    }
                       
                                                  });
                                                }
                                                else{
                                                  if(tlArray.length == 0){
                                                    if(ownerArray.length != 0){
                                                      var usersTobeSent = [cmResponse[0].cmId,ownerArray[0].id];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
                                                    else{
                                                      var usersTobeSent = [cmResponse[0].cmId];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
    
                                                  }
                                                  else{
    
                                                      app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                        if(tlError){
                                                          callback(tlError);
                                                        }
                                                        else{
                                                          app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                            var afterResult = [];
                                                            if(ownerArray.length != 0){
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,ownerArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
                                                            else{
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
    
                                                          });
                                                        }
                                                      });
                                                  }
          
                                                }
                                                
                                              });
                                            }
                                          }
                                        });
        
                                      }
                                      
                                    });
                                  });
                                  
                                  
                                }
                              });
      
                            }
                            else{
                            
                              User.find({where:{id: cmResponse[0].cmId}},function(userError,userResponse){
                                app.models.assignlead.findOrCreate({where:{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id}},{propertyId:propertyId, userId:cmResponse[0].cmId, leadId : res.id, userType:"CM",userFirstName:userResponse[0].userFirstName,userUnique:cmResponse[0].cmUnique},function(assignleadError,assignleadResponse){
                                  if(assignleadError){
                                    callback(assignleadError);
                                  }
                                  else{
                                    
                                    Admingroup.find({include: 'lakshyaUser',where:{propertyId:propertyId}},function(findUsersError,findUsersResponse){
                                      if(findUsersError){
                                        callback(findUsersError);
                                      }
                                      else{
                                        if(findUsersResponse != null){
                                          //console.log("TL");
                                          //console.log(findUsersResponse);
                                          var seArray = [];
                                          var otherUserArray = [];
                                          var ownerArray = [];
                                          var countIndex = 0;
                                          
                                          recursiveFindUser(0,findUsersResponse,seArray,otherUserArray,ownerArray,function(seArray,tlArray){
                                            //console.log(seArray);
                                            if(seArray.length == 1){
                                              app.models.salesExecutive.find({where:{seId:seArray[0].id}},function(seError,seResponse){
                                                if(seError){
                                                  callback(seError);
                                                }
                                                else{
                                                  if(tlArray.length == 0){
                                                    if(ownerArray.length != 0){
                                                      var usersTobeSent = [cmResponse[0].cmId,ownerArray[0].id];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
                                                    else{
                                                      var usersTobeSent = [cmResponse[0].cmId];
                                                      callback(null,{"success":true,"users":usersTobeSent});
                                                    }
        
                                                  }
                                                  else{
                                                    app.models.assignlead.findOrCreate({where:{propertyId,userId:seArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:seArray[0].id, leadId : res.id, userType:"SE",userFirstName:seArray[0].userFirstName,userUnique:seResponse[0].seUnique},function(assignSeError,assignSeResponse){
                                                      app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                        if(tlError){
                                                          callback(tlError);
                                                        }
                                                        else{
                                                          app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                            if(ownerArray.length != 0){
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id,ownerArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
                                                            else{
                                                              var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,seArray[0].id];
                                                              callback(null,{"success":true,"users":usersTobeSent});
                                                            }
                                                            
                                                          });
                                                        }
                                                      });
                                                    //callback(null,{"success":true});
    
                                                    });
                                                  }
                                                }
                   
                                              });
                                            }
                                            else{
                                              if(tlArray.length == 0){
                                                if(ownerArray.length != 0){
                                                  var usersTobeSent = [cmResponse[0].cmId,ownerArray[0].id];
                                                  callback(null,{"success":true,"users":usersTobeSent});
                                                }
                                                else{
                                                  var usersTobeSent = [cmResponse[0].cmId];
                                                  callback(null,{"success":true,"users":usersTobeSent});
                                                }
    
                                              }
                                              else{
                                                app.models.teamLeader.find({where:{tlId:tlArray[0].id}},function(tlError,tlResponse){
                                                  if(tlError){
                                                    callback(tlError);
                                                  }
                                                  else{
                                                    app.models.assignlead.findOrCreate({where:{propertyId,userId:tlArray[0].id, leadId:res.id}},{propertyId:propertyId, userId:tlArray[0].id, leadId : res.id, userType:"TL",userFirstName:tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique},function(assignTlError,assignTlResponse){
                                                      if(ownerArray.length != 0){
                                                        var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id,ownerArray[0].id];
                                                        callback(null,{"success":true,"users":usersTobeSent});
                                                      }
                                                      else{
                                                        var usersTobeSent = [cmResponse[0].cmId,tlArray[0].id];
                                                        callback(null,{"success":true,"users":usersTobeSent});
                                                      }
    
                                                    });
                                                  }
                                                });
                                              }
                                            }
                                            
                                          });
                                        }
                                      }
                                    });
                                    
    
                                  }
                                  
                                });
    
                              });
                            }
                            
                          }
                          
                        });
                      }
                    });
                  }
                  
                
                });
              });
            }
          


           
		   	}
	  });
	};

   Lead.remoteMethod('openNewLead', {
       accepts: [
         {
           "arg": "propertyId",
           "type": "string",
           "required": true,
           "description": ""
         },
         {
           "arg": "leadEmail",
           "type": "string",
           "required": true,
           "description": ""
         },
         {
           "arg": "leadFirstName",
           "type": "string",
           "required": true,
           "description": ""
         },
         {
           "arg": "leadContact",
           "type": "string",
           "required": true,
           "description": ""
         },
         {
           "arg": "leadSourceName",
           "type": "string",
           "required": true,
           "description": ""
         },
         {
           "arg": "leadLastName",
           "type": "string",
           "required": true,
           "description": ""
         },
         {
          "arg": "leadWebsiteForm",
          "type": "string",
          "description": ""
         },
         {
          "arg": "leadReferenceName",
          "type": "string",
          "description": ""
         },
         {
          "arg": "leadReferenceNumber",
          "type": "string",
          "description": ""
         },
         {
          "arg": "leadUserId",
          "type": "string",
          "description": ""
         }

       ],
       returns: {
           "arg": "result",
           "type": "object",
           "root": true,
           "description": ""
         },
         http: [
         {
           "path": "/openNewLead",
           "verb": "post"
         }
       ]
 });


 Lead.remoteMethod('leadNotification', {
  accepts: [],
    returns: {
        "arg": "result",
        "type": "object",
        "root": true,
        "description": ""
      },
      http: [
      {
        "path": "/leadNotification",
        "verb": "get"
      }
    ]
  });

};
