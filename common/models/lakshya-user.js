'use strict';
var loopback = require('loopback');
module.exports = function(Lakshyauser) {

Lakshyauser.on('resetPasswordRequest', function(info,res) {

	var otp = Math.floor(100000 + Math.random() * 900000);
	var html = `Welcome to Vastu Real Estate. <br>`+otp+` this is your OTP for verification.<br> Thank you for registering with us.`;

	var Lakshyausercollection = Lakshyauser.getDataSource().connector.collection(Lakshyauser.modelName);


	Lakshyausercollection.find({email:info.email}).toArray(function(err, user) {
		if(err || user.length==0){
			var result={"success":false};
			console.log(err);
			//res.render(result);
		}else{
			user[0].otp=String(otp);
			Lakshyausercollection.update({email:info.email},user[0], function(err, user) {
			if (err) {
					var result={"success":false};
					//res.render(result);
					console.log(err);

				}else{
				  Lakshyauser.app.models.sendMail.send({
				    to: info.email,
				    from: info.email,
				    subject: 'Vastu User Registration OTP',
				    html: html
				  }, function(err) {
				    if (err) {
					console.log(err);
				    	var result={"success":false};
				    	//res.render(result);
				    }else{
						var result={"success":true,message:"passwword changed"};
					//	res.render(result);
				    }
				  });
			
				}
			});

		}
 	});
});




Lakshyauser.afterRemote('login', function(ctx, res, next) {

	var Notification = loopback.getModel('notification');
	let app = require('../../server/server');
                                                
console.log(ctx.args.credentials.userNotificationToken);
	if(ctx.args.credentials.userNotificationToken){
	Notification.destroyAll( {notificationToken: ctx.args.credentials.userNotificationToken.toString() }, function(e,r){
		console.log(e,'errrrrrrrrrrrrrr',r,'resssssssssssssssssss');
		Notification.create({userId:res.userId,notificationToken:ctx.args.credentials.userNotificationToken}, function(e,r){
			next();
		});
	});
	}else{
		next();
	}
});




Lakshyauser.resetUserPassword = function(otp,password,callback) {

	var Lakshyausercollection = Lakshyauser.getDataSource().connector.collection(Lakshyauser.modelName);
console.log('1');
	Lakshyausercollection.find({otp:otp}).toArray(function(err, user) {
		if (err || user.length!=1) {
	console.log('2');		callback(err);
		}else{
			user[0].otp = "";
			user[0].password = Lakshyauser.hashPassword(password);
			user[0].emailVerified = true;
console.log('3');
		  	Lakshyausercollection.update({email:user[0].email},user[0], function(err, user) {
				if (err) {
					callback(err);console.log('4');
				}else{
				  var result={"success":true,message:"passwword changed"};console.log('5');
				  callback(null,result);
				}
			});
	
		}
	});
};

Lakshyauser.remoteMethod('resetUserPassword', {
    accepts: [
		{
			"arg": "otp",
			"type": "string",
			"required": true,
			"description": "",
			
		},
		{
			"arg": "password",
			"type": "string",
			"required": true,
			"description": ""
		}
    ],
    returns: {
        "arg": "result",
        "type": "object",
        "root": true,
        "description": ""
      },
      http: [
      {
        "path": "/resetUserPassword",
        "verb": "post"
      }
    ]
});



};


