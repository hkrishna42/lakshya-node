'use strict';

module.exports = function(Assignlead) {
    //var Changelog = require('loopback').getModel('changelog');
    Assignlead.postAssignLead = function(cmArray,tlArray,seArray,callback){
        let app = require('../../server/server'); 
        //console.log(cmArray);
        //console.log(tlArray);
        //console.log(seArray);
        Assignlead.find({where:{"userType":'SE',"propertyId":seArray[0].propertyId}},function(findAssignLeadError,findAssignLeadResponse){
            if(findAssignLeadError){
                console.log(findAssignLeadError);
                callback(findAssignLeadError);
            }
            else{
                console.log(findAssignLeadResponse.length);
                if(findAssignLeadResponse.length == 0){
                    //console.log(cmArray[0]);
                    app.models.clusterManager.find({where:{cmId:cmArray[0].userId}},function(cmError,cmResponse){
                        Assignlead.findOrCreate({where:{userType:'CM',propertyId:cmArray[0].propertyId,leadId:cmArray[0].leadId}},{userId:cmArray[0].userId,userType:'CM',propertyId:cmArray[0].propertyId,leadId:cmArray[0].leadId,userFirstName :cmArray[0].userFirstName,userUnique:cmResponse[0].cmUnique },function(cmInsertError,cmInsertResponse){
                            if(cmInsertError){
                                console.log(cmInsertError);
                                callback(cmInsertError);
                            }
                            else{
                                //console.log(cmInsertResponse);
                                app.models.teamLeader.find({where:{tlId:tlArray[0].userId}},function(tlError,tlResponse){
                                    Assignlead.findOrCreate({where:{userType:'TL',propertyId:tlArray[0].propertyId,leadId:tlArray[0].leadId}},{userId:tlArray[0].userId,userType:'TL',propertyId:tlArray[0].propertyId,leadId:tlArray[0].leadId,userFirstName :tlArray[0].userFirstName,userUnique:tlResponse[0].tlUnique },function(tlInsertError,tlInsertResponse){
                                        if(tlInsertError){
                                            console.log(tlInserError);
                                        }
                                        else{
                                            app.models.salesExecutive.find({where:{seId:seArray[0].userId}},function(seError,seResponse){                                            
                                                Assignlead.findOrCreate({where:{userType:'SE',propertyId:seArray[0].propertyId,leadId:seArray[0].leadId}},{userId:seArray[0].userId,userType:'SE',propertyId:seArray[0].propertyId,leadId:seArray[0].leadId,userFirstName :seArray[0].userFirstName,userUnique:seResponse[0].seUnique },function(seInsertError,seInsertResponse){
                                                    if(seInsertError){
                                                        console.log(seInsertError);
                                                    }
                                                    else{
                                                        console.log(seInsertResponse);
                                                        //Changelog.
                                                        callback(null,{"success":true});
                                                    }
                                                });
    
                                            });
                                            
                                        }
                                    });
                                });

                            }
                        });
                    });
                }else{
                    //console.log();
                    Assignlead.destroyAll({userType:'SE',propertyId:seArray[0].propertyId,"leadId":seArray[0].id},function(assignLeadError,assignLeadResponse){
                        if(assignLeadError){
                            console.log(assignLeadError);
                        }
                        else{
                            console.log(assignLeadResponse);
                            app.module.clusterManager.find({where:{cmId:cmArray[0].userId}},function(cmError,cmResponse){
                                Assignlead.findOrCreate({where:{userType:'CM',propertyId:cmArray[0].propertyId,leadId:cmArray[0].leadId}},{userId:cmArray[0].userId,userType:'CM',propertyId:cmArray[0].propertyId,leadId:cmArray[0].leadId,userFirstName :cmArray[0].userFirstName,userUnique:cmResponse[0].cmUnique },function(cmInsertError,cmInsertResponse){
                                    if(cmInsertError){
                                        console.log(cmInsertError);
                                        callback(cmInsertError);
                                    }
                                    else{
                                        //console.log(cmInsertResponse);
                                        app.module.teamLeader.find({where:{tlId:tlArray[0].userId}},function(tlError,tlResponse){
                                            Assignlead.findOrCreate({where:{userType:'TL',propertyId:tlArray[0].propertyId,leadId:tlArray[0].leadId}},{userId:tlArray[0].userId,userType:'TL',propertyId:tlArray[0].propertyId,leadId:tlArray[0].leadId,userFirstName :tlArray[0].userFirstName,userUnique : tlResponse[0].tlUnique },function(tlInsertError,tlInsertResponse){
                                                if(tlInsertError){
                                                    console.log(tlInsertError);
                                                }
                                                else{
                                                    app.module.salesExecutive.find({where:{seId:seArray[0].userId}},function(seError,seResponse){    
                                                        Assignlead.findOrCreate({where:{userType:'SE',propertyId:seArray[0].propertyId,leadId:seArray[0].leadId}},{userId:seArray[0].userId,userType:'SE',propertyId:seArray[0].propertyId,leadId:seArray[0].leadId,userFirstName:seArray[0].userFirstName, userUnique : seResponse[0].seUnique},function(seInsertError,seInsertResponse){
                                                            if(seInsertError){
                                                                console.log(seInsertError);
                                                            }
                                                            else{
                                                                console.log(seInsertResponse);
                                                                callback(null,{"success":true});
                                                            }
                                                        });
                                                    });
                                                    //console.log(tlInsertResponse);
                                                    //callback(null,tlInsertResponse);
                                                    
                                                }
                                            });
                                        });
            
                                    }
                                });
                            });
                        }
                    });
                }
                /*Assignlead.destroyAll({where:{"userType":'SE',"propertyId":seArray[0].propertyId}},function(assignLeadError,assignLeadResponse){
                    if(assignLeadError){
                        console.log(assignLeadError);
                    }
                    else{
                        console.log(assignLeadResponse);
                    }
                });*/
        
            }
        });
        
    }

    Assignlead.remoteMethod('postAssignLead', {
		accepts: [
				{
					"arg": "cmArray",
					"type": "array",
					"required": true,
					"description": ""
                },
                {
                    "arg": "tlArray",
					"type": "array",
					"required": true,
					"description": ""
                },
                {
                    "arg": "seArray",
					"type": "array",
					"required": true,
					"description": ""
                }
		],
			returns: {
					"arg": "result",
					"type": "object",
					"root": true,
					"description": ""
				},
				http: [
				{
					"path": "/postAssignLead",
					"verb": "post"
				}
			]
    });
};
