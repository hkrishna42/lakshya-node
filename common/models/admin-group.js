'use strict';

module.exports = function(Admingroup) {

  
  Admingroup.getUserProperties = function(userId, callback) {

    Admingroup.find({include: 'lakshyaProperty',where:{userId:userId}}, function(err,res) { 
      if (err) {
        callback(err);
      }else{
        callback(null,res);
      }
    });
  };

  Admingroup.assignProperties = function(userId,propertyId,callback){
    Admingroup.findOrCreate({where:{userId:userId,propertyId:propertyId}},{userId:userId,propertyId:propertyId},function(adminGroupError,adminGroupResponse){
      if(adminGroupError){
        callback(adminGroupError)
      }
      else{
        callback(null,adminGroupResponse);
      }
    });
  }

  Admingroup.assignPropertyToSalesExecutives = function(userIdArray,propertyId,callback){
    //console.log(userIdArray);
    //console.log(propertyId);
    //Admingroup.findOrCreate({})
    recursiveSefindOrCreate(userIdArray,userIdArray.length,0,propertyId,function(result){
      if(!result){
        callback(result);
      }
      else{
        callback(null,{"success":true});
      }
    });
    /*Admingroup.destroyAll([where], function(e,r){
      
    });*/

    
  }

  var recursiveSefindOrCreate = function(userIdArray,userIdArrayLength,index,propertyId,callback){
    if(userIdArrayLength == index){
      callback(true)
    }
    else{
      Admingroup.findOrCreate({where:{userId:userIdArray[index],propertyId:propertyId}},{userId:userIdArray[index],propertyId:propertyId},function(adminGroupError,adminGroupResponse){
        if(adminGroupError){
          callback(false);
        }
        else{
          recursiveSefindOrCreate(userIdArray,userIdArrayLength,index+1,propertyId,callback);
        }
      });
    }
  }



  Admingroup.remoteMethod('getUserProperties', {
        accepts: [
          {
            "arg": "userId",
            "type": "string",
            "required": true,
            "description": "",
            'http': {source: 'query'}
          }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/getUserProperties",
            "verb": "get"
          }
        ]
  });

  Admingroup.remoteMethod('assignProperties', {
    accepts: [
      {
        "arg": "userId",
        "type": "string",
        "required": true,
        "description": ""
      },
      {
        "arg": "propertyId",
        "type": "string",
        "required": true,
        "description": ""
      }

      
    ],
    returns: {
        "arg": "result",
        "type": "object",
        "root": true,
        "description": ""
      },
      http: [
      {
        "path": "/assignProperties",
        "verb": "post"
      }
    ]
});
  Admingroup.remoteMethod('assignPropertyToSalesExecutives', {
    accepts: [
      
      {
        "arg": "userIdArray",
        "type": "array",
        "required": true,
        "description": ""
      },
      {
        "arg": "propertyId",
        "type": "string",
        "required": true,
        "description": ""
      }
      
    ],
    returns: {
        "arg": "result",
        "type": "object",
        "root": true,
        "description": ""
      },
      http: [
      {
        "path": "/assignPropertyToSalesExecutives",
        "verb": "post"
      }
    ]
});


};
