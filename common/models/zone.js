'use strict';

module.exports = function(Zone) {

    Zone.validatesUniquenessOf('zipcodes');



    Zone.getDistinctPincode = function(callback) {

        Zone.find({}, function(err,res) { 
        if (err) {
            callback(err);
        }else{
            var index = 0;
            var resultArray = [];
            while(index < res.length){
                console.log(res[index]);
                var pincodeIndex = 0;
                while(pincodeIndex < res[index].zipcodes.length){
                    resultArray.push(
                        res[index].zipcodes[pincodeIndex]
                    );
                    pincodeIndex = pincodeIndex + 1;
                }
                
                index = index + 1;
            }
            callback(null,removeDups(resultArray));
        }
        });
    };

  Zone.remoteMethod('getDistinctPincode', {
       
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/getDistinctPincode",
            "verb": "get"
          }
        ]
  });

  function removeDups(names) {
    let unique = {};
    names.forEach(function(i) {
      if(!unique[i]) {
        unique[i] = true;
      }
    });
    return Object.keys(unique);
  }

};
