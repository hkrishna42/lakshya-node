'use strict';
var loopback = require('loopback');

module.exports = function(Cart) {

    Cart.insertPropertyToCart = function(userId,propertyId,callback){
        Cart.find({where:{userId : userId}},function(countError,countResponse){
            if(countError){
                callback(null,{"message":"Something went wrong!"});
            }
            else{
                if(countResponse.length < 9){
                    Cart.findOrCreate({where:{userId:userId,propertyId:propertyId}},{userId:userId,propertyId:propertyId},function(cartError,cartResponse){
                        if(cartError){
                            callback(null,{"message":"Something went wrong!"});
                        }
                        else{
                            callback(null,{"message":"One Property Added To Cart","count":countResponse.length+1});
                        }
                    });
                }
                else{
                    callback(null,{"message":"Greater Than 9"});
                }
            }
        });
        
    }

    Cart.getProperties = function(userId,callback){
        Cart.find({where:{userId : userId},include:'property'},function(countError,countResponse){
            if(countError){
		console.log(countError);
                callback(null,{"message":"Something went wrong!"});
            }
            else{
                var response = [];
		//console.log(countResponse);
                for (var i = countResponse.length - 1; i >= 0; i--) {
                    response.push(countResponse[i].property());
                };
                callback(null,response);
            }
        });
    }


    Cart.removePropertyFromCart = function(userId,propertyId,callback){
	var Cart = loopback.getModel('cart');

        Cart.destroyAll({userId:userId,propertyId:propertyId}, function(e,r){
            if(e){
                callback(null,{"message":"Something went wrong!","success":false});
            }
            else{
                callback(null,{"message":"One Property Removed From Cart","success":true});
            } 
        });
    }

    Cart.remoteMethod('insertPropertyToCart', {
	    accepts: [
				{
					"arg": "userId",
					"type": "string",
					"required": true,
					"description": ""
                },
                {
                    "arg": "propertyId",
					"type": "string",
					"required": true,
					"description": ""
                },
		],
			returns: {
					"arg": "result",
					"type": "object",
					"root": true,
					"description": ""
				},
				http: [
				{
					"path": "/insertPropertyToCart",
					"verb": "post"
				}
			]
    });

    Cart.remoteMethod('removePropertyFromCart', {
        accepts: [
                {
                    "arg": "userId",
                    "type": "string",
                    "required": true,
                    "description": ""
                },
                {
                    "arg": "propertyId",
                    "type": "string",
                    "required": true,
                    "description": ""
                },
        ],
            returns: {
                    "arg": "result",
                    "type": "object",
                    "root": true,
                    "description": ""
                },
                http: [
                {
                    "path": "/removePropertyFromCart",
                    "verb": "post"
                }
            ]
    });

    Cart.remoteMethod('getProperties', {
        accepts: [
                {
                    "arg": "userId",
                    "type": "string",
                    "required": true,
                    "description": ""
                }
        ],
            returns: {
                    "arg": "result",
                    "type": "object",
                    "root": true,
                    "description": ""
                },
                http: [
                {
                    "path": "/getProperties",
                    "verb": "post"
                }
            ]
    });
};

