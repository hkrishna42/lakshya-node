'use strict';

module.exports = function(Country) {
    Country.validatesUniquenessOf('countryName');
};
