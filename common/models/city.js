'use strict';

module.exports = function(City) {
    City.validatesUniquenessOf('cityName');
    var https = require('https');
    City.getCityArea = function(latlng,cityName,googleServerKey,callback){
        var Url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+cityName+'&types=geocode&location='+latlng+'&radius=1000&key='+googleServerKey;
        https.get(Url, function(response) {
        
        //console.log('STATUS: ' + response.statusCode);
        //console.log('HEADERS: ' + JSON.stringify(response.headers));
        
            response.setEncoding('utf8');
            var data = '';
            response.on('data', function (chunk) {
                //console.log('BODY: ' + chunk);
                data += chunk;
                
            });
            
            response.on('end',function(){
                
                //console.log(data);
                callback(null,JSON.parse(data));
            })
            .on("error", function(e){
                console.log("Got error: " + e.message);
                callback(e);
            });
        })
    }

    City.remoteMethod('getCityArea', {
		accepts: [
				{
					"arg": "latlng",
					"type": "string",
					"required": true,
                    "description": "",
                    'http': {source: 'query'}
                },
                {
                    "arg": "cityName",
					"type": "string",
					"required": true,
                    "description": "",
                    'http': {source: 'query'}
                },
                {
                    "arg": "googleServerKey",
					"type": "string",
					"required": true,
                    "description": "",
                    'http': {source: 'query'}
                }
		],
			returns: {
					"arg": "result",
					"type": "object",
					"root": true,
					"description": ""
				},
				http: [
				{
					"path": "/getCityArea",
					"verb": "get"
				}
			]
    });
};
