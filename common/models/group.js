'use strict';
var loopback = require('loopback');

module.exports = function(Group) {
/**
 * 
 * @param {string} propertyId 
 * @param {string} userId 
 * @param {Function(Error, object)} callback
 */

  Group.openNewGroup = function(propertyId, userId, callback) {
    console.log(propertyId,userId);
	var Lead = loopback.getModel('lead');
    var Leadcollection = Group.getDataSource().connector.collection(Lead.modelName);

	Lead.find( {where: { propertyId:propertyId,leadUserId: userId} },function(e,r){
        if (e) {
            callback(e);
        }else{console.log(r);
        	if (r.length>0) {
			    Group.findOrCreate({where: { propertyId: propertyId, userId: userId, leadId: r[0].id } }, { propertyId: propertyId, userId: userId, leadId: r[0].id }, function(err, res){
			    	if (err) {
			  	 	callback(err);
			    	}else{
			    		callback(null,res);
			    	}
			    });
        	}else{

//var Message = loopback.getModel('message');
//var Messagecollection = Message.getDataSource().connector.collection(Message.modelName);

//Message.destroyAll([], function(e,r){});
//var Groupcollection = Group.getDataSource().connector.collection(Group.modelName);
//Group.destroyAll([], function(e,r){});

			console.log(r);
	    		callback(null,{message:false});
        	}
        }
    });
    

  };


 Group.getPropertyUsers = function(propertyId,userId, callback) {
    var Assignlead = loopback.getModel('assignlead');
    var Assignleadcollection = Assignlead.getDataSource().connector.collection(Assignlead.modelName);

    Group.find({include: 'lakshyaUser',where:{propertyId:propertyId}}, function(err,res) {
      if (err) {
        callback(err);
      }else{
        var users = [];
        for (var i = res.length - 1; i >= 0; i--) {
console.log('lead',res[i].leadId);                
users.push(res[i]['leadId']);
        }
console.log('users',users);
        Assignleadcollection.find( { leadId: { $in: users } }).toArray(function(e,r){
	      	if (err) {
	  	 	 callback(err);
	      	}else{
		var result=[];
	      		console.log(r,res);
	      		for (var i = r.length - 1; i >= 0; i--) {
				
	      			for (var k = res.length - 1; k >= 0; k--) {
	      				console.log(res[k].leadId,r[i].leadId, r[i].userId,userId);
					if (String(res[k].leadId) == String(r[i].leadId) && String(r[i].userId)==String(userId)) {
						
	      					result.push({ propertyId:r[i].propertyId, leadId:r[i].leadId, lakshyaUser:res[k].lakshyaUser(), time:res[k].time });
	      				}
	      			}
	      		}
	console.log(result);
		        callback(null,Array.from(new Set(result)));
	      	}
		});
 
      }
    });
  };

  Group.remoteMethod('getPropertyUsers', {
        accepts: [
          {
            "arg": "propertyId",
            "type": "string",
            "required": true,
            "description": "",
            'http': {source: 'query'}
          },
          {
            "arg": "userId",
            "type": "string",
            "required": true,
            "description": "",
            'http': {source: 'query'}
          }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/getPropertyUsers",
            "verb": "get"
          }
        ]
  });

  Group.remoteMethod('openNewGroup', {
        accepts: [
          {
            "arg": "propertyId",
            "type": "string",
            "required": true,
            "description": ""
          },
          {
            "arg": "userId",
            "type": "string",
            "required": true,
            "description": ""
          }
        ],
        returns: {
            "arg": "result",
            "type": "object",
            "root": true,
            "description": ""
          },
          http: [
          {
            "path": "/openNewGroup",
            "verb": "post"
          }
        ]
  });

};



