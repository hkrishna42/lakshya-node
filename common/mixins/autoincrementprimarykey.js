module.exports = function(Model, options) {
    var modelName = Model.definition.name;     //Get the Model Name from Model Instance
    Model.observe('before save', function (ctx, next) {
        if (!ctx.isNewInstance) {
            next();
        }else{
            Model.getDataSource().connector.connect(function (err, db) {
                console.log(modelName);
                var collection = db.collection('counter');
                if(modelName == "lead"){
                    //console.log("Hello");
                    var identity = 'leadUnique';
                }
                if(modelName == 'clusterManager'){
                    console.log();
                    var identity = 'cmUnique';
                }
                if(modelName == 'lakshyaProperty'){
                    var identity = 'propertyUnique';
                }
                if(modelName == 'teamLeader'){
                    var identity = 'tlUnique';
                }
                if(modelName == 'salesExecutive'){
                    var identity = 'seUnique';
                }
                if(modelName == 'externalConsultant'){
                    var identity = 'ecUnique';
                }
                collection.findAndModify({name: modelName}, [[identity, 'asc']], {$inc: {value: 1}}, { new: true, upsert: true }, function (err, rec) {
                    if (err) {
                        console.err(err);
                        next();
                    } else {
                        getPrimaryKeyFromModel(Model, function(primaryKey){
                            console.log(ctx.instance);
                            if (ctx.instance) {
                                //console.log("primarykey : "+ rec.value.value);
                                ctx.instance[primaryKey]   = rec.value.value;
                            } else {
                                ctx.data[primaryKey]  = rec.value.value;
                            }
                            next();
                        });
                    }
            
                });
            });
        }
    });
}

//Get the Primary Key from Model
var getPrimaryKeyFromModel = function(Model,cb){
    var properties = Model.definition.rawProperties;
    Object.keys(properties).forEach(function(key) {
        //console.log(properties[key].uniqueid);
        if(properties[key].uniqueid === true){
            //console.log(key);
            cb(key);
        }
    });
}